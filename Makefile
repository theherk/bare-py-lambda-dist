BUILD = build
TMP = tmp

$(BUILD)/dist.zip: $(BUILD) $(TMP)
	pip install --install-option="--prefix=$(TMP)" -r requirements.txt -I
	cp -r $(TMP)/lib*/*/site-packages/* $(BUILD)
	cp handler.py $(BUILD)
	rm -rf $(BUILD)/boto*
	cd $(BUILD) && zip -rq $@ . *

$(BUILD):
	mkdir -p $@

$(TMP):
	mkdir -p $@
